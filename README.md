# pdf417fontgen

This tool can be used to generate a PDF417 font that matches [this specification](https://grandzebu.net/informatique/codbar-en/pdf417.htm). The
font offered on that site is licensed as GPL, which implies that documents
embedding the font [should also be licensed using the GPL](https://en.wikipedia.org/wiki/GPL_font_exception) as well.

## Notes

To replicate the original font exactly, both the *Typo line gap* and the
*HHead Line Gap* need to set to 50. Unfortunately, ``svg2ttf`` has these values
hard coded (e.g. see ``hhea.js`` and ``sfnt.js``). So, either you need to edit these values
using e.g. FontForge (Element > Font info > OS/2 > Metrics), or, you need to
tweak ``svg2ttf`` internals a bit.


## License

The tool itself is [MIT](https://opensource.org/licenses/MIT) licensed. There
are no licensing restrictions on the font created by running the tool itself.
